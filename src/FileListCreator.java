import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*If the servent is sharing files, this will create a list of file names
* in the shared directory and then add the file names and sizes to the
* sharedFiles list.*/

public class FileListCreator extends Thread{
    private String filepath;
    private String slash;

    public FileListCreator(String filepath){
        this.filepath = filepath;
        if(filepath.contains("/")){
            slash = "/";
        }
        else{
            slash = "\\";
        }
    }

    public void run(){
        //create a file object
        File file = new File(filepath);
        //list of file names in shared file
        String[] filelist = file.list();
        //go through the list of file names and get their file sizes
        //then add to the sharedFiles list
        if(filelist == null){
            System.out.println("Can not find shared files at " + filepath);
        }
        gnutella.sharedfiles.clear();
        for(String str : filelist){
            String filesize = filepath + slash + str;
            Path path = Paths.get(filesize);
            long bytes = 0;
            try {
                bytes = Files.size(path);
            } catch (IOException e) {
                e.printStackTrace();
            }

            SharedFile newfile = new SharedFile(str,bytes);
            gnutella.sharedfiles.add(newfile);
            //System.out.println("Added " + newfile.getFileName());
        }
        System.out.println("File list build complete");
        /*for(SharedFile obj : gnutella.sharedfiles){
            System.out.println("Filename: "+obj.getFileName()+" File Size: "+obj.getFileSize());
        }*/
    }
}
