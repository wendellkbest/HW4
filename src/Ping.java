import java.time.LocalTime;
import java.util.UUID;
/**This class is just a ping object*/
public class Ping {
    private UUID messageID;
    private LocalTime timeReceived;
    private String thisIP;
    private int thisPort;

    public Ping(UUID id, LocalTime time, String ip, int port){
        messageID = id;
        timeReceived = time;
        thisIP = ip;
        thisPort = port;
    }

    public UUID getMessageID(){return messageID;}
    public LocalTime getTimeReceived(){return timeReceived;}
    public String getThisIP(){return thisIP;}
    public int getThisPort(){return thisPort;}
}
