/*This class performs the function of pinging the host. If this servent
* is not sharing files, it will just ping the host it initially connects to.
* If it is sharing files, it will need to ping all of the servents that have
* sent a pong to this servent*/

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**This thread will run continuously sending out a Ping according to the value in the Thread Sleep
 * method. The main method generates the random message ID. A helper method is used to build all
 * header files in the Gnutella 0.4 format.*/

public class PingBroadcast extends Thread{
    private InetAddress gnutellaHost;
    private int gnutellaHostPort;
    private Ping ping;
    private UUID send;

    private boolean sentPong;
    //Constructor used when connecting to another servent
    public PingBroadcast(Ping p,String hostIP, int port) throws UnknownHostException, SocketException {
        ping = p;
        gnutellaHost = InetAddress.getByName(hostIP);
        gnutellaHostPort = port;
        send = p.getMessageID();
        sentPong = false;
    }
    //Constructor used when sending pings to all servents
    public PingBroadcast(Ping p){
        ping = p;
        send = p.getMessageID();
        gnutellaHost = null;
        gnutellaHostPort = 0;
        sentPong = false;
    }
    // Sends a 16 byte Message ID
    // 1 byte Payload descriptor 0x00
    // 1 byte TTL 0x07
    // 1 byte Hops 0x00
    // 4 byte payload length
    public void run(){
        byte[] descriptor = new byte[0];
        while(true){
            try {
                descriptor = headerBuilder(ping);
                if(gnutella.pingHashMap.isEmpty() && gnutellaHostPort != 0 && gnutellaHost != null){
                    PingSender send = new PingSender(ping,descriptor, gnutellaHost, gnutellaHostPort);
                    send.start();
                }
                else{
                    if(!gnutella.pingHashMap.isEmpty()){
                        Collection<Ping> pings = gnutella.pingHashMap.values();
                        Iterator<Ping> pingItr = pings.iterator();
                        while(pingItr.hasNext()){
                            Ping p = pingItr.next();
                            PingSender send = new PingSender(ping,descriptor,InetAddress.getByName(p.getThisIP()),p.getThisPort());
                            send.start();
                        }
                    }
                }
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    /**Helper method for the class only that builds the byte array for the Gnutella header*/
    private byte[] headerBuilder(Ping p) throws UnknownHostException {
        byte[] descriptor = new byte[29];
        //convert UUID to byte array
        ByteBuffer uuid = ByteBuffer.wrap(new byte[16]);
        uuid.putLong(p.getMessageID().getMostSignificantBits());
        uuid.putLong(p.getMessageID().getLeastSignificantBits());
        byte[] uuidArr = uuid.array();
        //add UUID to descriptor
        int i = 0;
        for(byte b : uuidArr){
            descriptor[i++]= b;
        }
        //add command
        descriptor[i++] = 0x00;
        //add TTL
        descriptor[i++] = gnutella.TTLInittial;
        //Hops
        descriptor[i++] = gnutella.HopInitVal;
        //payload
        int payload = 6;
        i = i + 3;
        descriptor[i--] = (byte)(payload & 0xFF);
        payload >>= 8;
        descriptor[i--] = (byte)(payload & 0xFF);
        payload >>= 8;
        descriptor[i--] = (byte)(payload & 0xFF);
        payload >>= 8;
        descriptor[i] = (byte)(payload & 0xFF);
        i = i + 4;
        //IP address
        InetAddress ad = InetAddress.getByName(p.getThisIP());
        byte[] ip = ad.getAddress();
        for(byte b : ip){
            descriptor[i++] = b;
        }
        //Port
        int port = p.getThisPort();
        i = i + 1;
        descriptor[i--] = (byte)(port & 0xFF);
        port >>= 8;
        descriptor[i] = (byte)(port & 0xFF);
        return descriptor;
    }
}
