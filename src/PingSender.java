import java.io.IOException;
import java.net.*;

/** This class just sends out a ping through UDP. Runs once.*/
public class PingSender extends Thread{
    private byte[] descriptor;
    private DatagramSocket socket;
    private InetAddress hostIP;
    private int hostPort;
    //Constructor for sending a ping with raw data
    public PingSender(InetAddress hostip, int hostport, byte[] arr) throws SocketException {
        descriptor = new byte[arr.length];
        descriptor = arr.clone();
        socket = new DatagramSocket();
        hostIP = hostip;
        hostPort = hostport;
    }
    //constructor for sending pings to pongs
    public PingSender(Pong pongTemp, byte[] arr) throws SocketException, UnknownHostException {
        descriptor = new byte[arr.length];
        descriptor = arr.clone();
        socket = new DatagramSocket();
        hostIP = InetAddress.getByName(pongTemp.getIpAddress());
        hostPort = pongTemp.getPort();
    }
    public PingSender(Ping pingTemp, byte[] arr, InetAddress host, int port) throws SocketException, UnknownHostException {
        descriptor = new byte[arr.length];
        descriptor = arr.clone();
        socket = new DatagramSocket();
        hostIP = host;
        hostPort = port;
    }

    public void run(){
        //System.out.println("Sending ping to " + hostIP + " port "+ hostPort);
        DatagramPacket packet = new DatagramPacket(descriptor, descriptor.length, hostIP, hostPort);
        try {
            socket.send(packet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
