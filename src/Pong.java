import java.util.UUID;
/**This is just a class for holding the pong values*/
public class Pong {

    private UUID messageid; //a unique ID that must match a ping ID
    private int numberOfFile; //number of files shared
    private long sizeOfSharedFiles; //number of kilobytes shared
    private int port;
    private String ipAddress;

    public Pong(UUID messageid, int port, String ipaddress, int numberoffiles, long sizeoffiles){
        this.messageid = messageid;
        this.port = port;
        ipAddress = ipaddress;
        numberOfFile = numberoffiles;
        sizeOfSharedFiles = sizeoffiles;
    }

    public UUID getMessageid(){return messageid;}
    public int getPort(){return port;}
    public String getIpAddress(){return ipAddress;}
    public int getNumberOfFile(){return numberOfFile;}
    public long getSizeOfSharedFiles(){return sizeOfSharedFiles;}

    public void setNumberOfFiles(int size) {
        numberOfFile = size;
    }

    public void setFilesSize(long sum) {
        sizeOfSharedFiles = sum;
    }
}
