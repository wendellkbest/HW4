import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;

/**This class just sends a pong. It works for sending pongs from the servent
 * and sending pongs from other servents. If the pong is being sent by the
 * servent, it updates the number and size of files before sending pong*/
public class PongSender extends Thread{
    private Pong pong;
    InetAddress hostIP;
    int hostPort;
    private DatagramSocket socket = new DatagramSocket();

    public PongSender(Pong p, InetAddress hostIP, int hostport) throws SocketException {
        pong = p;
        //This part is for automatically updating this servent's file numbers and size
        long sum = 0;
        if(p.getNumberOfFile()==0 && p.getMessageid() == gnutella.pingPongMessageID){
            pong.setNumberOfFiles(gnutella.sharedfiles.size());
            for(int i = 0; i < gnutella.sharedfiles.size(); i++){
                sum = gnutella.sharedfiles.get(i).getFileSize();
            }
            pong.setFilesSize(sum);
        }
       this. hostIP = hostIP;
        hostPort = hostport;
    }

    public PongSender(Pong p) throws SocketException {
        pong = p;
        //This part is for automatically updating this servent's file numbers and size
        long sum = 0;
        if(p.getNumberOfFile()==0 && p.getMessageid() == gnutella.pingPongMessageID){
            pong.setNumberOfFiles(gnutella.sharedfiles.size());
            for(int i = 0; i < gnutella.sharedfiles.size(); i++){
                sum = gnutella.sharedfiles.get(i).getFileSize();
            }
            pong.setFilesSize(sum);
        }
        this. hostIP = null;
        hostPort = 0;
    }

    public void run(){
        try {
            byte[] descriptor = this.headerBuilder(pong);
            //Sends a pong to specific servent
            if(hostIP != null && hostPort != 0){
                DatagramPacket packet = new DatagramPacket(descriptor, descriptor.length, hostIP,hostPort );
                socket.send(packet);
                //System.out.println("Sent pong with UUID "+ pong.getMessageid());
            }
            else{
                Collection<Pong> pongs = gnutella.pongHashMap.values();
                Iterator<Pong> pongItr = pongs.iterator();
                while(pongItr.hasNext()){
                    Pong tempPong = pongItr.next();
                    //System.out.println("Pong address " + tempPong.getIpAddress());
                    DatagramPacket packet = new DatagramPacket(descriptor, descriptor.length, InetAddress.getByName(tempPong.getIpAddress()), tempPong.getPort());
                    socket.send(packet);
                    //System.out.println("Sent pong with UUID "+ tempPong.getMessageid());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //helper function for building header and payload
    private byte[] headerBuilder(Pong p) throws UnknownHostException {
        //convert UUID to byte array
        ByteBuffer uuidb = ByteBuffer.wrap(new byte[16]);
        uuidb.putLong(p.getMessageid().getMostSignificantBits());
        uuidb.putLong(p.getMessageid().getLeastSignificantBits());
        byte[] uuidArr = uuidb.array();
        //the header array for the ping
        byte[] descriptor = new byte[37];
        //build the ping header, add UUID to front
        int i = 0;
        for(; i<uuidArr.length; i++){
            descriptor[i] = uuidArr[i];
        }
        //Ping command
        descriptor[i++] = 0x01;
        //Time to live
        descriptor[i++] = gnutella.TTLInittial;
        //Hops
        descriptor[i++] = gnutella.HopInitVal;
        //payload size
        int payloadsize = 16;
        i = i + 3;
        descriptor[i--] = (byte) (payloadsize & 0xFF);
        payloadsize >>= 8;
        descriptor[i--] = (byte) (payloadsize & 0xFF);
        payloadsize >>= 8;
        descriptor[i--] = (byte) (payloadsize & 0xFF);
        payloadsize >>= 8;
        descriptor[i] = (byte) (payloadsize & 0xFF);
        i = i + 4;
        //port
        int port = p.getPort();
        i = i + 1;
        descriptor[i--] = (byte)(port & 0xff);
        port >>= 8;
        descriptor[i] = (byte)(port & 0xff);
        i = i + 2;
        //ip address
        InetAddress ad = InetAddress.getByName(p.getIpAddress());
        byte[] ip = ad.getAddress();
        for(byte b : ip){
            descriptor[i++] = b;
        }
        //number of shared files
        int numFiles = p.getNumberOfFile();
        i = i + 3;
        descriptor[i--] = (byte)(numFiles & 0xFF);
        numFiles >>=8;
        descriptor[i--] = (byte)(numFiles & 0xFF);
        numFiles >>=8;
        descriptor[i--] = (byte)(numFiles & 0xFF);
        numFiles >>=8;
        descriptor[i] = (byte)(numFiles & 0xFF);
        i = i + 4;
        //number of kilobytes shared
        long size = p.getSizeOfSharedFiles();
        i = i + 3;
        descriptor[i--] = (byte)(size & 0xFF);
        size >>= 8;
        descriptor[i--] = (byte)(size & 0xFF);
        size >>= 8;
        descriptor[i--] = (byte)(size & 0xFF);
        size >>= 8;
        descriptor[i] = (byte)(size & 0xFF);
        return descriptor;
    }
}
