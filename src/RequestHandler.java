import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**This class waits requests from other servents then initiates a
 * RequestImplementer to  */
public class RequestHandler extends Thread{
    private DatagramSocket socket;
    private byte[] buf = new byte[2048];
    public RequestHandler(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    public void run(){
        System.out.println("RequestHandler running");
        while(true){
            DatagramPacket packet = new DatagramPacket(buf,buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            //System.out.println("Received request");
            RequestImplementer newPack = new RequestImplementer(packet);
            newPack.start();
        }
    }
}
