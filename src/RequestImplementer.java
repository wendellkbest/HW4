import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalTime;
import java.util.*;
/**Handles any request sent by RequestHandler.*/
public class RequestImplementer extends Thread{
    private DatagramPacket newPacket;

    public RequestImplementer(DatagramPacket packet){
        newPacket = packet;
    }

    public void run(){
        //System.out.println("RequestImplementor running");
        byte[] dataArr = newPacket.getData();
        try {
            if(dataArr[16] == 0x00){ this.pingRequest(dataArr);} //ping 0x00
            else if(dataArr[16] == 0x01){this.pongRequest(dataArr);}//pong 0x01
            else if(dataArr[16] == 0x40){this.pushRequest(dataArr);}//Push 0x40
            else if(dataArr[16] == 0x70){System.out.println("Received Query Request");this.queryRequest(dataArr);}//Query 0x70
            else if(dataArr[16] == 0x71){System.out.println("Received QueryHit request"); this.queryHitRequest(dataArr);}//Queryhit 0x71
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    /**When a servent receives a new servent ping, it just records the message ID and
     * the time the ping was received. The ping just serves as a mechanism to determine
     * if the servent is a recognized member of the group. No need to send a pong unless
     * the servent is sharing files. Servents wishing to download files must accept sharing
     * the files they download*/
    private void pingRequest(byte[] arr) throws SocketException, UnknownHostException {
        byte[] adjust = arr.clone();
        InetAddress ping = InetAddress.getByAddress(Arrays.copyOfRange(arr,23,27));
        String pingip = ping.getHostName();
        int queryPort = (int)this.convertByteArrToLong(Arrays.copyOfRange(arr,27, 29), 0,1);
        byte[] temp = Arrays.copyOfRange(arr,0,16);
        Ping ptemp = new Ping(this.convertToUUID(temp), LocalTime.now(),pingip,queryPort);
        //System.out.println(ptemp.getMessageID() + " " + ptemp.getTimeReceived());
        //receive the ping and either add it or update the ping reception time
        if(ptemp.getMessageID().compareTo(gnutella.pingPongMessageID)!=0){
            if(gnutella.pingHashMap.containsKey(ptemp.getMessageID()) && adjust[17] != 0){
                gnutella.pingHashMap.replace(ptemp.getMessageID(), ptemp);
                //System.out.println("PingHashMap size is "+gnutella.pingHashMap.size());
            }
            else{
                gnutella.pingHashMap.put(ptemp.getMessageID(), ptemp);
                //System.out.println("PingHashMap size is "+gnutella.pingHashMap.size());
            }
            //Forward ping to the other servents
            if(!gnutella.pingHashMap.isEmpty() && adjust[17] != 0){
                Collection<Ping> pingCollection = gnutella.pingHashMap.values();
                Iterator<Ping> pingIterator = pingCollection.iterator();
                while(pingIterator.hasNext()){
                    Ping pingTemp = pingIterator.next();
                    if(pingTemp.getMessageID() != ptemp.getMessageID()){
                        PingSender pping = new PingSender(pingTemp, arr,InetAddress.getByName(pingTemp.getThisIP()),pingTemp.getThisPort());
                        pping.start();
                    }
                }
                //System.out.println("Sent pings to servents");
            }

            Pong newpong = new Pong(gnutella.pingPongMessageID,gnutella.UDPPort,gnutella.thisIP, 0,0);
            PongSender sendPong = new PongSender(newpong,InetAddress.getByName(ptemp.getThisIP()),ptemp.getThisPort());
            sendPong.start();
        }
    }
    /**Pong
     * Case 1: Received a pong from another servent and the UUID matches
     * a UUID in the ping list. Add/update pong list then forward to other
     * members of pong list
     * Case 2: UUID of pong is not in ping list, ignore
     * Case 3: UUID matches this UUID, ignore*/
    private void pongRequest(byte[] arr) throws SocketException, UnknownHostException {
        byte[] temp = Arrays.copyOf(arr,16);
        UUID tempPong = this.convertToUUID(temp);
        int port = (int)this.convertByteArrToLong(arr,23,24);
        String ip = InetAddress.getByAddress(Arrays.copyOfRange(arr,25,29)).getHostAddress();
        int numFiles = (int)this.convertByteArrToLong(arr,29,32);
        int sizeFiles = (int)this.convertByteArrToLong(arr,33,36);
        //System.out.println("Received pong "+tempPong+" "+port+" "+ip+" "+numFiles+" "+sizeFiles);
        //See if pong request matches this servent
        Pong p = new Pong(tempPong,port,ip,numFiles,sizeFiles);
        if(tempPong.compareTo(gnutella.pingPongMessageID) != 0){
            gnutella.pongHashMap.put(tempPong,p);
        }
        else{
            if(gnutella.pingHashMap.containsKey(tempPong)){
                PongSender bob = new PongSender(p,InetAddress.getByName(gnutella.pingHashMap.get(tempPong).getThisIP()), gnutella.pingHashMap.get(tempPong).getThisPort());
                bob.start();
            }
        }
    }
    private void pushRequest(byte[] arr){}
    private void queryRequest(byte[] arr) throws IOException {
        boolean isInQueryHashMap = false;
        int numHits = 0;
        //get the UUID of request
        byte[] queryArr = Arrays.copyOf(arr,16);
        UUID queryUUID = this.convertToUUID(Arrays.copyOf(arr,16));
        //get the payload of request
        int payload = (int)convertByteArrToLong(arr,19,22);
        byte[] search = new byte[payload];
        for(int i = 23, j = 0; i < 23 + payload; i++, j++){
            search[j] = arr[i];
        }
        String query = new String(search);
        System.out.println("Query from " + queryUUID + " query: " + query);
        if(!gnutella.queryhashMap.containsKey(queryUUID) && !query.equals(gnutella.queryhashMap.get(queryUUID))){
            gnutella.queryhashMap.put(queryUUID,query);
        }
        else{
            isInQueryHashMap = true;
        }
        String results = "";
        for(int i = 0; i < gnutella.sharedfiles.size(); i++){
            SharedFile temp = gnutella.sharedfiles.get(i);
            if(temp.getFileName().contains(query)){
                results = results.concat(temp.getFileName() + "|");
                numHits+=1;
            }
        }
        if(!isInQueryHashMap){
            if(results != null){
                //Number of hits|port|IP address|Result Set| Servent ID
                InetAddress IrIP = InetAddress.getByName(gnutella.thisIP);
                byte[] rIP = IrIP.getAddress();
                //Find TCPIP port of this servent
                int rport = gnutella.TCPIPPort;
                int qport = gnutella.pingHashMap.get(queryUUID).getThisPort();
                byte[] rPORT = this.convertNumToByte(rport, 2);
                //Results of search seperated by "|"
                byte[] resultsArr = results.getBytes();
                //Converting this servent ID to byte[]
                ByteBuffer bytes = ByteBuffer.wrap(new byte[16]);
                bytes.putLong(gnutella.pingPongMessageID.getMostSignificantBits());
                bytes.putLong(gnutella.pingPongMessageID.getLeastSignificantBits());
                byte[] uuid = bytes.array();
                //Determining size of payload
                int newPayload = 1 + rPORT.length + 4 + resultsArr.length + uuid.length;
                //create descriptor for query hit
                byte[] descriptor = new byte[ queryArr.length + 7 + newPayload];
                //Put the UUID of the query
                int i = 0;
                for(byte b : queryArr){
                    descriptor[i++] = b;
                }
                //Query hit command
                descriptor[i++] = 0x71;
                //TTL
                descriptor[i++] = gnutella.TTLInittial;
                //Hops
                descriptor[i++] = gnutella.HopInitVal;
                //Payload size
                int index = i + 3;
                for(;index >= i; index--){
                    descriptor[index] = (byte)(newPayload & 0xFF);
                    newPayload >>= 8;
                }
                i+=4;
                //Number of hits
                descriptor[i++] = (byte) numHits;
                //Port
                for (byte b : rPORT){
                    descriptor[i++] = b;
                }
                //IP address
                for(byte b : rIP){
                    descriptor[i++] = b;
                }
                //results
                for(byte b : resultsArr){
                    descriptor[i++] = b;
                }
                //this servent ID
                for(byte b : uuid){
                    descriptor[i++] = b;
                }
                System.out.println("Sending query results");
                DatagramSocket socket = new DatagramSocket();
                DatagramPacket packet = new DatagramPacket(descriptor,descriptor.length,InetAddress.getByName(gnutella.pingHashMap.get(queryUUID).getThisIP()),qport);
                socket.send(packet);

                Collection<Pong> pongs = gnutella.pongHashMap.values();
                Iterator<Pong> ponsItr = pongs.iterator();
                while(ponsItr.hasNext()){
                    Pong p = ponsItr.next();
                    if(p.getMessageid().compareTo(queryUUID) != 0){
                    DatagramPacket sender = new DatagramPacket(arr, arr.length, InetAddress.getByName(p.getIpAddress()),p.getPort());
                    socket.send(sender);
                    }
                }
            }
            else{
                if(!gnutella.pongHashMap.isEmpty()){
                    DatagramSocket socket = new DatagramSocket();
                    Collection<Pong> pongs = gnutella.pongHashMap.values();
                    Iterator<Pong> ponsItr = pongs.iterator();
                    while(ponsItr.hasNext()){
                        Pong p = ponsItr.next();
                        DatagramPacket sender = new DatagramPacket(arr, arr.length, InetAddress.getByName(p.getIpAddress()),p.getPort());
                        socket.send(sender);
                    }
                }
            }
        }
    }
    private void queryHitRequest(byte[] arr) throws IOException {
        byte[] UUID = Arrays.copyOf(arr, 16);
        UUID request = this.convertToUUID(UUID);
        if(request.compareTo(gnutella.pingPongMessageID) == 0){
            int payload = (int)this.convertByteArrToLong(arr, 19, 22);
            byte[] requestPayload = new byte[payload];
            requestPayload = Arrays.copyOfRange(arr,23, 23 + payload);
            int numHits = requestPayload[0];
            int port =(int)this.convertByteArrToLong(requestPayload,1,2);
            byte[] responderIP = Arrays.copyOfRange(requestPayload, 3, 7);
            String ip = InetAddress.getByAddress(responderIP).getHostName();
            byte[] stringsArr = Arrays.copyOfRange(requestPayload,7, payload - 16);
            String temp = new String(stringsArr, StandardCharsets.UTF_8);
            byte[] rUUID = Arrays.copyOfRange(requestPayload, payload - 16, payload);
            UUID responseUUID = this.convertToUUID(rUUID);
            String[] hitResults = temp.split("\\|");
            System.out.println("Query results from " + responseUUID + " at IP "+ip+" port "+port+" number of files found "+numHits);
            for(String s : hitResults){
                System.out.println(s);
            }
        }
    }

    private UUID convertToUUID(byte[] arr){
        ByteBuffer bb = ByteBuffer.wrap(arr);
        long high = bb.getLong();
        long low = bb.getLong();
        UUID newUUID = new UUID(high, low);
        return newUUID;
    }

    private long convertByteArrToLong(byte[] arr, int start, int end){
        long result = 0;
        for(int i = start; i < end +1; i++){
            result = (result << 8) + (arr[i] & 0xFF);
        }
        return result;
    }
    private String convertByteToString(byte[] arr, int offset, int length){
        String result = new String(arr, offset, length);
        return result;
    }
    private byte[] convertNumToByte(int number, int size){
        byte[] temp = new byte[size];
        for(int i = size -1; i >= 0; i--){
            temp[i] = (byte)(number & 0xFF);
            number >>= 8;
        }
        return temp;
    }
}


