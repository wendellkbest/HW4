/**This class is an object for storing shared file information*/
public class SharedFile {
    private String fileName;
    private long fileSize;

    public SharedFile(String name, long size){
        fileName = name;
        fileSize = size;
    }

    public String getFileName(){return fileName;}
    public long getFileSize(){return fileSize;}
}
