import java.io.*;
import java.net.Socket;
import java.nio.Buffer;

/*This will query a servent to check if this servent has the same version, if not, the servent
* will exit the program so user can get new version. If version is correct, provides a method for main
* to determine whether it can continue running the Servent*/

public class VersionCheck extends Thread{
    private String version;
    private boolean result;
    private String hostIP;
    private int hostPort;
    private String response;

    private final String gnutellaConnect = "GNUTELLA CONNECT/";

    public VersionCheck(String version, String hostIP, int hostPort){
        this.version = version;
        this.hostIP = hostIP;
        this.hostPort = hostPort;
    }

    public void run(){
        Socket socket = null;
        OutputStream out = null;
        InputStream in = null;
        try {
            socket = new Socket(hostIP, hostPort);
            out = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(out,true);
            System.out.println("Sending version to servent");
            String send = gnutellaConnect+version+"\n\n";
            writer.println(send);

            in = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            response = reader.readLine();

            if(response.contains("GNUTELLA OK")){
                result = true;
            }
            else{
                result = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getResult(){return result;}
    public String getResponse(){return response;}
}
