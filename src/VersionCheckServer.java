import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/*This is a server thread that will do version checking for any servent wanting to join the group.
* Rejection means the requesting servent will need to use this versions servent to connect, acceptance means
* The requesting Servent can proceed with sending a ping. This will always be running.*/

public class VersionCheckServer extends Thread{
    private int port;
    private String version;
    private String gnutellaConnect = "GNUTELLA CONNECT/";

    public VersionCheckServer(int port, String version){
        this.port = port;
        this.version = version;
    }

    public void run(){
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true){

            try {
                Socket socket = serverSocket.accept();
                System.out.println("Servent request received");
                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                String versionCheck = reader.readLine();
                System.out.println("Given version is: "+versionCheck);
                OutputStream output = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(output, true);
                if(versionCheck.contains(gnutellaConnect+version)){
                    System.out.println("Servent version is correct");
                    writer.println("GNUTELLA OK\n\n");
                }
                else{
                    System.out.println("Servent version is not correct");
                    writer.println("Invalid version, need "+ version);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
