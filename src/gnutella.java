import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.time.LocalTime;
import java.util.*;

import static java.lang.System.exit;

/**Author: Wendell Best
*  April 19, 2022
* The main class for the gnutella server. **/


public class gnutella {
    /*Used to keep track of ser4vents who have pinged this servent*/
    public static HashMap<UUID, Ping> pingHashMap = new HashMap<>();
    /*Keep track of the servents that have pong this servent*/
    public static HashMap<UUID, Pong> pongHashMap = new HashMap<>();
    /*Keep track of query*/
    public static HashMap<UUID,String> queryhashMap = new HashMap<>();
    /*List of files available for upload, maybe empty if no files are to be shared*/
    public static ArrayList<SharedFile> sharedfiles = new ArrayList<>();
    /* Path to the shared folder */
    public static String sharedfolder = null;
    /* The message ID for ping and pong descriptors*/
    public static UUID pingPongMessageID;
    /* the version of this servent*/
    public static final String serventVersion = "0.4";
    /* TTL value */
    public static final byte TTLInittial = 0x07;
    public static final byte HopInitVal = 0x00;
    //command line flags are as follows
    public static String hostIP = null; // flag -hip need this to contact gnutella servent
    public static int hostPort = 0; // flag -hp need this to contact gnutella UDP servent
    public static int hostVersionPort = 0; // flag -hvp need this to send version check request
    public static String thisIP = null; // flag -sip the ip for this servent
    public static int versionPort = 0; // flag -svp the version port for this servent
    public static int UDPPort = 0; // flag -sup the UDP port for this servent to receive requests
    public static int TCPIPPort = 0; // flag -stp the TCP/IP port for file transfers

    /**
     * To start this servent, need to have the IP address of a servent already in the P2P network.
     * Need to provide the path to the folder where shared files will go. Then need to do a version check
     * and then proceed according to whether files wille be shared or not. IP addresses and port numbers
     * are entered at the command line through the use of flags. Per the Gnutella standard, Message ID
     * is in the UUID format
     **/

    public static void main(String[] args) throws IOException, InterruptedException {

        //retrieve flagged information from command line
        for (int str = 0; str < args.length; str++) {
            if (args[str].equals("-hip")) {
                hostIP = args[++str];
            } else if (args[str].equals("-hp")) {
                hostPort = Integer.parseInt(args[++str]);
            } else if (args[str].equals("-sip")) {
                thisIP = args[++str];
            } else if (args[str].equals("-svp")) {
                versionPort = Integer.parseInt(args[++str]);
            } else if (args[str].equals("-sup")) {
                UDPPort = Integer.parseInt(args[++str]);
            } else if (args[str].equals("-stp")) {
                TCPIPPort = Integer.parseInt(args[++str]);
            } else if (args[str].equals("-sf")) {
                sharedfolder = args[++str];
            } else if (args[str].equals("-hvp")) {
                hostVersionPort = Integer.parseInt(args[++str]);
            }
        }
        //for testing command line input
        System.out.println("Host IP: " + hostIP + " Host Port: " + hostPort +
                "Host version port " + hostVersionPort+ " This IP: " + thisIP +
                " Version port: " + versionPort + " UDP Port: " + UDPPort +
                " TCP Port " + TCPIPPort + " Shared File Path: " + sharedfolder);
        pingPongMessageID = UUID.randomUUID();
        System.out.println("Servent UUID is " + pingPongMessageID);
        //Checking if the version is correct, if it is, pong the contacted
        if (hostIP != null && hostVersionPort != 0) {
            System.out.println("Sending version check");
            VersionCheck find = new VersionCheck(serventVersion, hostIP, hostVersionPort);
            find.start();
            find.join();
            if (!find.getResult()) {
                System.err.println(find.getResponse());
                exit(0);
            } else {
                System.out.println("This servent matches version of host servent");
            }
        }

        //Start the server for version checking
        if (versionPort != 0) {
            //System.out.println("Starting Version Server");
            VersionCheckServer serverCheck = new VersionCheckServer(versionPort, serventVersion);
            serverCheck.start();
        }

        //build the list of files
        if (sharedfolder != null) {
            System.out.println("Building list of shared files");
            FileListCreator makelist = new FileListCreator(sharedfolder);
            makelist.start();
        }
        //Handles any requests from other servents
        if (UDPPort != 0) {
            //System.out.println("Starting RequestHandler");
            RequestHandler rh = new RequestHandler(UDPPort);
            rh.start();
        }

        //Start the ping broadcaster
        Ping p = new Ping(pingPongMessageID, LocalTime.now(),thisIP,UDPPort);
        if (hostIP != null && hostPort != 0) {
            //System.out.println("Starting ping server");
            PingBroadcast pingsender = new PingBroadcast(p,hostIP, hostPort);
            pingsender.start();
        }
        else{
            //System.out.println("Starting Ping Server");
            PingBroadcast pingsender = new PingBroadcast(p);
            pingsender.start();
        }
        String exit = "exit";
        boolean run = true;
        Scanner obj = new Scanner(System.in);
        while(run){
            System.out.println("Please Select From the following options");
            System.out.println("1. Retrieve list of pings");
            System.out.println("2. Retrieve list of pongs");
            System.out.println("3. See list of files");
            System.out.println("4. If files have been added/removed to/from share folder, use this option to refresh file list");
            System.out.println("5. Search for a file");
            System.out.println("Type exit to close the servent");
            String input = obj.nextLine();
            //Close the servent
            if(input.equals(exit)){
                System.out.println("Shutting down servent, goodbye");
                exit(0);
            }
            //See list of pings
            else if(input.equals("1")){
                if(pingHashMap.isEmpty()){
                    System.out.println("Ping list is empty");
                }
                else{
                    Collection<Ping> pings = pingHashMap.values();
                    Iterator<Ping> pingItr = pings.iterator();
                    while(pingItr.hasNext()){
                        Ping temp = pingItr.next();
                        System.out.println("Ping ID: " + temp.getMessageID() + " Received: " + temp.getTimeReceived());
                    }
                }
            }
            //see list of pongs
            else if(input.equals("2")){
                if(pongHashMap.isEmpty()){
                    System.out.println("Pong list is empty");
                }
                else{
                    Collection<Pong> pongs = pongHashMap.values();
                    Iterator<Pong> pongItr = pongs.iterator();
                    while(pongItr.hasNext()){
                        Pong temp = pongItr.next();
                        System.out.println("Pong ID: " + temp.getMessageid() + "\n From IP: " + temp.getIpAddress() + "\n Port: " + temp.getPort());
                    }
                }

            }
            //See list of files
            else if(input.equals("3")){
                if(sharedfiles.isEmpty()){
                    System.out.println("No files are being shared");
                }
                else{
                    long sum = 0;
                    for(int i = 0; i < sharedfiles.size(); i++){
                        System.out.println("File name: " + sharedfiles.get(i).getFileName() + " Size: " + sharedfiles.get(i).getFileSize());
                        sum = sum + sharedfiles.get(i).getFileSize();
                    }
                    System.out.println("Number of Files: " + sharedfiles.size() + " Total files size: " + sum);
                }

            }
            //Refresh shared folder files if any changes have been made
            else if(input.equals("4")){
                FileListCreator makelist = new FileListCreator(sharedfolder);
                makelist.start();
            }
            //Query
            else if(input.equals("5")){
                System.out.println("Please enter all or part of a file name");
                String filename = obj.nextLine();
                doSearchQuery(filename);
            }
            else{continue;}
        }
    }
    static void doSearchQuery(String query) throws IOException {
        if(!pongHashMap.isEmpty()){
            DatagramSocket socket = new DatagramSocket();
            byte[]descriptor = headerBuilder(query);
            Collection<Pong> pongs = pongHashMap.values();
            Iterator<Pong> pongsItr = pongs.iterator();
            while(pongsItr.hasNext()){
                Pong p = pongsItr.next();
                DatagramPacket packet = new DatagramPacket(descriptor, descriptor.length, InetAddress.getByName(p.getIpAddress()), p.getPort());
                socket.send(packet);
            }
        }
        else{
            System.out.println("No servents with files available for sharing");
        }
    }
    static private byte[] headerBuilder(String query){
        byte[] search = query.getBytes();
        ByteBuffer uuid = ByteBuffer.wrap(new byte[16]);
        uuid.putLong(pingPongMessageID.getMostSignificantBits());
        uuid.putLong(pingPongMessageID.getLeastSignificantBits());
        byte[] uuidArr = uuid.array();
        int payload = search.length;
        byte[] descriptor = new byte[uuidArr.length + 7 + payload];
        int i = 0;
        for(byte b : uuidArr){
            descriptor[i++] = b;
        }
        descriptor[i++] = 0x70;
        descriptor[i++] = TTLInittial;
        descriptor[i++] = HopInitVal;

        int index = i + 3;
        for(;index >= i; index--){
            descriptor[index] = (byte)(payload & 0xFF);
            payload >>= 8;
        }
            i = i + 4;
        for(byte b : search){
            descriptor[i++] = b;
        }
        return descriptor;
    }
}